/// @file
/// @brief Contains functions for reading PE file headers

#include <../include/pe_reader.h>
#include <stdio.h>
#include <string.h>

int seek_pe_header(FILE *in) {
    int result = fseek(in, OFFSET_TO_PE_SIGNATURE, SEEK_SET);
    if (result) {
        printf("ERR:FAIL_TO_SEEK_TO_PE_START\n");
        return 1;
    }

    uint32_t offset;
    size_t size = fread(&offset, 4, 1, in);
    if (size != 1) {
        printf("ERR:FILE_CONTAINS_LESS_BITES\n");
        return 2;
    }

    result = fseek(in, offset, SEEK_SET);
    if (result) {
        printf("ERR:FAIL_TO_SEEK_TO_OFFSET\n");
        return 3;
    }

    return 0;
}


int read_pe_header(FILE *in, struct PEHeader *header) {
    size_t size = fread(header, sizeof(struct PEHeader), 1, in);

    if (size != 1) {
        printf("ERR:PE_HEADER_CONTAINS_LESS_BITES\n");
        return 1;
    }

    return 0;
}


int read_section_table(FILE *in, struct SectionTable *section) {
    size_t size = fread(section, sizeof(struct SectionTable), 1, in);
    if (size != 1) {
        printf("ERR:SECTION_TABLE_CONTAINS_LESS_BITES\n");
        return 1;
    }

    return 0;
}

/// @brief Compute real length of section name in PE file.
/// @param name Section name to compute length of.
/// @return Real length of section name.
size_t section_name_length(const uint8_t *name) {
    for (size_t len = 0; len < MAX_SECTION_TABLE_NAME_LENGTH; len++) {
        if (name[len] == '\0') {
            return len;
        }
    }

    return MAX_SECTION_TABLE_NAME_LENGTH;
}

/// @brief Util function. Compare name to PE section name.
/// @param left_name Name to compare with
/// @param right_name PE section name to compare to
/// @return 0 if names are equal and 1 if they are not
int compare_section_names(char *left_name, uint8_t *right_name) {
    size_t len_1 = strlen(left_name);
    size_t len_2 = section_name_length(right_name);

    if (len_1 != len_2) {
        return 1;
    }

    if (memcmp(left_name, right_name, len_1) != 0) {
        return 1;
    }

    return 0;
}

/// @brief Util function. Copy part of one file into another.
/// @param in File co copy from
/// @param out File to copy into
/// @param size Amount of bits to copy
/// @return 0 in case of success or error code
int copy_file_section(FILE *in, FILE *out, size_t size) {
    uint8_t buffer;
    uint32_t st;
    for (size_t i = 0; i < size; i += sizeof(buffer)) {
        st = fread(&buffer, sizeof(buffer), 1, in);
        if (st) {
            fwrite(&buffer, sizeof(buffer), 1, out);
        } else {
            return 1;
        }
    }

    return 0;
}

int copy_section(FILE *in, char *section_name, FILE *out) {
    if (seek_pe_header(in)) {
        printf("ERR:CANT_SEEK_PE_HEADER\n");
        return ERR_CANT_SEEK_PE_HEADER;
    }

    struct PEHeader header;
    if (read_pe_header(in, &header)) {
        printf("ERR:CANT_READ_PE_HEADER\n");
        return ERR_CANT_READ_PE_HEADER;
    }

    int result = fseek(in, header.SizeOfOptionalHeader, SEEK_CUR);
    if (result) {
        printf("ERR:CANT_SEEK_OPTIONAL_HEADER\n");
        return ERR_CANT_SEEK_OPTIONAL_HEADER;
    }

    struct SectionTable section;
    size_t i;
    for (i = 0; i < header.NumberOfSections; i++) {
        if (read_section_table(in, &section)) {
            printf("ERR:CANT_READ_%zu_SECTION_TABLE\n", i);
            return ERR_CANT_READ_SECTION_TABLE;
        }

        if (compare_section_names(section_name, section.Name) == 0) {
            break;
        }
    }
    if (i == header.NumberOfSections) {
        printf("ERR:CANT_FIND_SECTION_TABLE: %s\n", section_name);
        return ERR_CANT_FIND_SECTION_TABLE;
    }

    result = fseek(in, section.PointerToRawData, SEEK_SET);
    if (result) {
        printf("ERR:CANT_SEEK_TO_RAW_DATA\n");
        return ERR_CANT_SEEK_TO_RAW_DATA;
    }

    result = copy_file_section(in, out, section.SizeOfRawData);
    if (result) {
        printf("ERR:CANT_COPY\n");
        return ERR_CANT_COPY;
    }

    printf("STATUS:SUCCESS\n");
    return STATUS_SUCCESS;
}
