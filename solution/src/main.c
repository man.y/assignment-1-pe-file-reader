/// @file 
/// @brief Main application file

#include <pe_reader.h>
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// \return return #RETURN_STATUS value enum
int main(int argc, char **argv) {


    if (argc < 1) {
        printf("ERR:ARGC_LESS_THAN_ONE\n");

        return ERR_ARGC_LESS_THAN_ONE;
    }

    if (argc < 4) {
        printf("ERR:NOT_ENOUGH_ARGC\n");
        usage(stdout);
        return ERR_NOT_ENOUGH_ARGC;
    }

    FILE *in;
    in = fopen(argv[1], "rb");
    if (!in) {
        printf("ERR:IN_FILE_NOT_OPENED\n");
        return ERR_IN_FILE_NOT_OPENED;
    }

    FILE *out;
    out = fopen(argv[3], "wb");
    if (!out) {
        fclose(in);
        printf("ERR:OUT_FILE_NOT_OPENED\n");
        return ERR_OUT_FILE_NOT_OPENED;
    }

    int status = copy_section(in, argv[2], out);
    fclose(in);
    fclose(out);

    return status;
}
