/// @file
/// @brief Contains data structs for PEHeader and SectionTable and some utils definitions for reading them.

#pragma once
#ifndef SECTION_EXTRACTOR_PE_READER_H
/// @brief Include guard
#define SECTION_EXTRACTOR_PE_READER_H

#include <stdio.h>

/*! @brief After the MS-DOS stub, at the file offset specified at offset 0x3c,
    is a 4-byte signature that identifies the file as a PE format image file.
*/
#define OFFSET_TO_PE_SIGNATURE 0x3c

/*! @brief Executable images do not use a string table and do not support
    section names longer than 8 characters. Long names in object files are
    truncated if they are emitted to an executable file.
*/
#define MAX_SECTION_TABLE_NAME_LENGTH 8

#ifdef _MSC_VER
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#else

#include <stdint.h>

#endif

/*! \struct PEHeader

    This structure stores data of standard COFF file header.
*/

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __GNUC__
        __attribute__((packed))
#endif
PEHeader {
    /// @brief Start label of COFF file header data.
    uint8_t magic[4];
    /// @brief The number that identifies the type of target machine.
    uint16_t Machine;
    /// @brief The number of sections. This indicates the size of the section table, which immediately follows the headers.
    uint16_t NumberOfSections;
    /// @brief The low 32 bits of the number of seconds since 00:00 January 1, which indicates when the file was created.
    uint32_t TimeDateStamp;
    /// @brief The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
    uint32_t PointerToSymbolTable;
    /// @brief The number of entries in the symbol table.
    uint32_t NumberOfSymbols;
    /// @brief The size of the optional header.
    uint16_t SizeOfOptionalHeader;
    /// @brief The flags that indicate the attributes of the file.
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/*! \struct SectionTable

    Each row of the section table is, in effect, a section header.
    The number of entries in the section table is given by the NumberOfSections field in the file header.
    Entries in the section table are numbered starting from one (1).
    For a total of 40 bytes per entry.
*/

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __GNUC__
        __attribute__((packed))
#endif
SectionTable {
    /*! @brief The name of the section. For longer names, this field contains a slash (/) that is
        followed by an ASCII representation of a decimal number that is an offset into the string table.
    */
    uint8_t Name[8];
    /*! @brief For executable images, the address of the first byte of the section relative to the image base
        when the section is loaded into memory. For object files, this field is the address of the first byte
        before relocation is applied; for simplicity, compilers should set this to zero. Otherwise, it is an
        arbitrary value that is subtracted from offsets during relocation.
    */
    uint32_t VirtualSize;
    /*! @brief The size of the section (for object files) or the size of the initialized data on disk
        (for image files). For executable images, this must be a multiple of FileAlignment from the optional
        header. If this is less than VirtualSize, the remainder of the section is zero-filled. Because the
        SizeOfRawData field is rounded but the VirtualSize field is not, it is possible for SizeOfRawData to be
        greater than VirtualSize as well. When a section contains only uninitialized data, this field should be zero.
    */
    uint32_t VirtualAddress;
    /*! @brief The size of the section (for object files) or the size of the initialized data on disk (for image
        files). For executable images, this must be a multiple of FileAlignment from the optional header. If this is
        less than VirtualSize, the remainder of the section is zero-filled. Because the SizeOfRawData field is rounded
        but the VirtualSize field is not, it is possible for SizeOfRawData to be greater than VirtualSize as well.
        When a section contains only uninitialized data, this field should be zero.
    */
    uint32_t SizeOfRawData;
    /*! @brief The file pointer to the first page of the section within the COFF file. For executable images, this
        must be a multiple of FileAlignment from the optional header. For object files, the value should be aligned
        on a 4-byte boundary for best performance. When a section contains only uninitialized data, this field
        should be zero.
    */
    uint32_t PointerToRawData;
    /// @brief The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    uint32_t PointerToRelocations;
    /*! @brief The file pointer to the beginning of line-number entries for the section. This is set to zero if there are
        no COFF line numbers. This value should be zero for an image because COFF debugging information is deprecated.
    */
    uint32_t PointerToLinenumbers;
    /// @brief The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t NumberOfRelocations;
    /// @brief The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
    uint16_t NumberOfLinenumbers;
    /// @brief The flags that describe the characteristics of the section.
    uint32_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/// @brief Status codes that the program outputs. With success 0, in case of failure 1-11.
enum RETURN_STATUS {
    STATUS_SUCCESS, ///< 0
    ERR_CANT_SEEK_PE_HEADER, ///< 1
    ERR_CANT_READ_PE_HEADER, ///< 2
    ERR_CANT_SEEK_OPTIONAL_HEADER, ///< 3
    ERR_CANT_READ_SECTION_TABLE, ///< 4
    ERR_CANT_FIND_SECTION_TABLE, ///< 5
    ERR_CANT_SEEK_TO_RAW_DATA, ///< 6
    ERR_CANT_COPY, ///< 7
    ERR_ARGC_LESS_THAN_ONE, ///< 8
    ERR_NOT_ENOUGH_ARGC, ///< 9
    ERR_IN_FILE_NOT_OPENED, ///< 10
    ERR_OUT_FILE_NOT_OPENED, ///< 11
};

/// @brief Move in the input stream to PE header.
/// @param in is PE file in which to move to the section.
/// @return 0 in case of success.
/// @return 1 ERR:FAIL_TO_SEEK_TO_PE_START
/// @return 2 ERR:FILE_CONTAINS_LESS_BITES
/// @return 3 ERR:FAIL_TO_SEEK_TO_OFFSET
int seek_pe_header(FILE *in);

/// @brief Read PE header in the PE file.
/// @param in is File to read from.
/// @param header Struct to read into.
/// @return 0 in case of success or error code.
/// @return 1 ERR:PE_HEADER_CONTAINS_LESS_BITES
int read_pe_header(FILE *in, struct PEHeader *header);

/// @brief Read one section table in PE file.
/// @param in File to read from.
/// @param section Struct to read into.
/// @return 0 in case of success
/// @return 1 ERR:SECTION_TABLE_CONTAINS_LESS_BITES.
int read_section_table(FILE *in, struct SectionTable *section);

/// @brief Copy raw data of section in PE file into another file
/// @param in PE file to copy from
/// @param section_name Name of section in PE file
/// @param out File to copy into
/// @return 0 STATUS:SUCCESS
/// @return 1 ERR:CANT_SEEK_PE_HEADER
/// @return 2 ERR:CANT_READ_PE_HEADER
/// @return 3 ERR:CANT_SEEK_OPTIONAL_HEADER
/// @return 4 ERR:CANT_READ_%zu_SECTION_TABLE
/// @return 5 ERR:CANT_FIND_SECTION_TABLE
/// @return 6 ERR:CANT_SEEK_TO_RAW_DATA
/// @return 7 ERR:CANT_COPY
int copy_section(FILE *in, char *section_name, FILE *out);

#endif //SECTION_EXTRACTOR_PE_READER_H
